// import userModel from './models/user';

export const STORAGE_KEY = 'gestprog-storage-key';

// for testing
if (navigator.userAgent.indexOf('PhantomJS') > -1) {
  window.localStorage.clear();
}

export const state = {
  // c'est pour le storage dans le browser
  localStorageObjects: JSON.parse(window.localStorage.getItem(STORAGE_KEY) || '[]'),
  // c'est pour les donnees en RAM
  user: {},
};

// export const getters = {
//   getUser: (state) => {
//     state.user;
//   },
// };
