import Vue from 'vue';
import Router from 'vue-router';
import FormulaireCompetence from '@/components/form/competence/FormulaireCompetence';
import FormulaireChampCompetence from '@/components/form/champ-competence/FormulaireChampCompetence';
import Profil from '@/components/form/profil/Profil';
import Activite from '@/components/form/activite/ActiviteControlPanel';
import FormulaireCible from '@/components/form/cible/FormulaireCible';
import FormulaireSignUpUser from '@/components/form/user/SignUp';
import FormulaireLoginUser from '@/components/form/user/LogIn';
import TermsAndConditions from '@/components/static_rendering/TermsAndConditions';
import Accueil from '@/components/static_rendering/Welcome';
import Cours from '@/components/form/cours/CoursControlPanel';


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Accueil,
    },
    {
      path: '/Index',
      component: Accueil,
    },
    {
      path: '/FormulaireCible/:idProfil',
      component: FormulaireCible,
      props: { idProfil: true, sidebar: false },
    },
    {
      path: '/Profil',
      component: Profil,
    },
    {
      path: '/ChampCompetence',
      component: FormulaireChampCompetence,
    },
    {
      path: '/FormulaireCompetence',
      component: FormulaireCompetence,
    },
    {
      path: '/FormulaireCompetence/:idComp',
      component: FormulaireCompetence,
      props: { idComp: true, sidebar: false },
    },
    {
      path: '/FormulaireCible',
      component: FormulaireCible,
    },
    {
      path: '/Activite/:idProfil',
      component: Activite,
      props: { idProfil: true, sidebar: false },
    },
    {
      path: '/Activite',
      component: Activite,
    },
    {
      path: '/SignUp',
      component: FormulaireSignUpUser,
    },
    {
      path: '/LogIn',
      component: FormulaireLoginUser,
    },
    {
      path: '/TermsAndConditions',
      component: TermsAndConditions,
    },
    {
      path: '/Cours',
      component: Cours,
    },
    {
      path: '/Cours/:idProfil',
      component: Cours,
      props: { idProfil: true, sidebar: false },
    },
  ],
});
