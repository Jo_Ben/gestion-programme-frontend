// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import authService from './services/auth-service';

router.beforeEach((to, from, next) => {
  // const notLogged = authService.isLogged();
  // const notLogged = Object.keys(user).length === 0
  // && user.constructor === Object;
  // const notLogged = Object.keys(store.state.user).length === 0
  //   && store.state.user.constructor === Object;
  const notLogged = authService.tokenIsInvalid();
  // const notLogged = true;
  // todo ajouter le login et les autres pages qui peuvent etre accedees sans auth
  const navigateToPath = to.path === '/'
    || to.path === '/Index'
    || to.path === '/LogIn'
    || to.path === '/SignUp';
  if (!notLogged) {
    next();
  } else if (notLogged && navigateToPath) {
    next();
  } else {
    next('/LogIn');
  }
});

Vue.config.productionTip = false;
const VueAutosize = require('vue-autosize');

Vue.use(VueAutosize);

// export default {
//   bus: new Vue(),
// };
/* eslint-disable no-new */

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
